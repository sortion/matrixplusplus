#include "matrix.hpp"

void matrix(unsigned int HEIGHT, unsigned int WIDTH, string color, float speed, string charset)
{
    char **matrice = (char **) malloc(HEIGHT * sizeof(char *));
    // Alloc each row
    for (int i = 0; i < HEIGHT; i++)
    {
        matrice[i] = (char *) malloc(WIDTH * sizeof(char));
    }
    // Init blank matrix
    for (int i = 0; i < HEIGHT; i++)
    {
        for (int j = 0; j < WIDTH; j++)
        {
            matrice[i][j] = ' ';
        }
    }
    string COLOR = getcolor(color);
    cout << COLOR;
    show_matrix(matrice, HEIGHT, WIDTH, COLOR);
    float microsecond = speed * 1000000;
    while (true)
    {
        update_matrix(matrice, HEIGHT, WIDTH, charset);
        show_matrix(matrice, HEIGHT, WIDTH, COLOR);
        usleep(microsecond);
    }
}

void update_matrix(char **matrice, unsigned int HEIGHT, unsigned int WIDTH, string charset)
{
    int i, j;
    double rd;
    char **previous = (char **) malloc(HEIGHT * sizeof(char *));
    // Alloc each row
    for (int i = 0; i < HEIGHT; i++)
    {
        previous[i] = (char *) malloc(WIDTH * sizeof(char));
    }
    // Copy each value into previous matrice copy
    for (i = 0; i < HEIGHT; i++)
    {
        for (j = 0; j < WIDTH; j++)
        {
            previous[i][j] = matrice[i][j];
        }
    }
    // Generate new top row
    i = 0;
    for (j = 0; j < WIDTH; j++)
    {
        rd = rand() % 100;
        if (previous[i][j] == ' ')
        {
            if (rd > 95)
            {
                rd = rand() % charset.length();
                const char c = charset[rd];
                matrice[i][j] = c;
            } else 
            {
                matrice[i][j] = ' ';
            }
        } else {
            if (rd > 75)
            {
                matrice[i][j] = ' ';
            } else
            {
                rd = rand() % charset.length();
                const char c = charset[rd];
                matrice[i][j] = c;
            }
        }
    } 
    // Shift matrice by a single row
    for (i += 1; i < HEIGHT; i++)
    {
        for (j = 0; j < WIDTH; j++)
        {
            matrice[i][j] = previous[i-1][j];
        }
    }
}

void show_matrix(char **matrice, unsigned int HEIGHT, unsigned int WIDTH, string COLOR)
{
    system("clear");
    for (int i = 0; i < HEIGHT; i++)
    {
        for (int j = 0; j < WIDTH; j++)
        {
            cout << matrice[i][j];
        }
        cout << endl;
    }
}

void free_matrix(char **matrice, unsigned int HEIGHT, unsigned int WIDTH)
{
    for (int i = 0; i < HEIGHT; i++)
    {
        free(matrice[i]);
    }
    free(matrice);
}

string getcolor(string color)
{
    if (color == "default")
        return RST;
    else if (color == "red")
        return KRED;
    else if (color == "green")
        return KGRN;
    else if (color == "yellow")
        return KYEL;
    else if (color == "blue")
        return KBLU;
    else if (color == "magenta")
        return KMAG;
    else if (color == "cyan")
        return KCYN;
    else if (color == "white")
        return KWHT;
    else 
        return RST; 
}