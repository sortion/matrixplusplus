#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <ncurses.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <time.h>
#include <string>

using namespace std;

#define RST  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

void matrix(unsigned int HEIGHT, unsigned int WIDTH, string color, float speed, string charset);

void show_matrix(char **matrice, unsigned int HEIGHT, unsigned int WIDTH, string COLOR);

void update_matrix(char **matrice, unsigned int HEIGHT, unsigned int WIDTH, string charset);

void free_matrix(char **matrice, unsigned int HEIGHT, unsigned int WIDTH);

string getcolor(string color);

#endif