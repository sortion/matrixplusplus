# Install `matrix++`

## Build from source

### Requirements

* cmake
* gcc-g++
* gdb (optional)
* ncurses

### Procedure

```bash
# Clone repository
git clone https://framagit.org/UncleSamulus/matrixplusplus.git
cd matrixplusplus
# Build
make
```

## Run

```bash
./matrix++
```

## Install

```bash
make install
```
